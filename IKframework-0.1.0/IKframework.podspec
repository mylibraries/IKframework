Pod::Spec.new do |s|
  s.name = "IKframework"
  s.version = "0.1.0"
  s.summary = "IKframework."
  s.license = {"type"=>"MIT", "file"=>"LICENSE"}
  s.authors = {"user001"=>"tlq20110808@hotmail.com"}
  s.homepage = "https://github.com/user001/IKframework"
  s.description = "TODO: Add long description of the pod here."
  s.source = { :path => '.' }

  s.ios.deployment_target    = '8.0'
  s.ios.vendored_framework   = 'ios/IKframework.framework'
end
