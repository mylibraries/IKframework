# IKframework

[![CI Status](https://img.shields.io/travis/user001/IKframework.svg?style=flat)](https://travis-ci.org/user001/IKframework)
[![Version](https://img.shields.io/cocoapods/v/IKframework.svg?style=flat)](https://cocoapods.org/pods/IKframework)
[![License](https://img.shields.io/cocoapods/l/IKframework.svg?style=flat)](https://cocoapods.org/pods/IKframework)
[![Platform](https://img.shields.io/cocoapods/p/IKframework.svg?style=flat)](https://cocoapods.org/pods/IKframework)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

IKframework is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'IKframework'
```

## Author

user001, tlq20110808@hotmail.com

## License

IKframework is available under the MIT license. See the LICENSE file for more info.

#### 项目介绍
爱客公共类

#### 软件架构
软件架构说明


#### 安装教程

1. xxxx
2. xxxx
3. xxxx

#### 使用说明

1. xxxx
2. xxxx
3. xxxx

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [http://git.mydoc.io/](http://git.mydoc.io/)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)

