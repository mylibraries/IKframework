//
//  IKAppDelegate.h
//  IKframework
//
//  Created by user001 on 08/14/2018.
//  Copyright (c) 2018 user001. All rights reserved.
//

@import UIKit;

@interface IKAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
