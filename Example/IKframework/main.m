//
//  main.m
//  IKframework
//
//  Created by user001 on 08/14/2018.
//  Copyright (c) 2018 user001. All rights reserved.
//

@import UIKit;
#import "IKAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([IKAppDelegate class]));
    }
}
